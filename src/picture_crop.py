from PIL import Image

#img is image, w is desired width crop, h is desired height crop.
def picture_crop(img,w,h):
    #width is height of image, height is height of image
    width, height = img.size
    cropped_img = img.crop(((width-w)//2, (height-h)//2, (width+w)//2, (height+h)//2))
    return cropped_img