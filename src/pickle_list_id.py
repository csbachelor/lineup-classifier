import json
import requests
import pickle
from src.find_pos_id import find_pos_id



def pickle_list_id(map, x, y, z, name='save.p'):
    ##### load smokes for a position
    list_id = pickle.load(open(name,'rb'))
    map_use = str(map)
    id = find_pos_id(map,x,y,z)
    if id in list_id:
        print(f'{id} already parsed')
        return
    data = {"mapName":map_use,"predefinedPositionId":id,"throwType":"standing","tickrate":128}
    
    r = requests.post(
        'https://scope.gg/api/grenades/getGrenadesForPredefinedPosition',
        data=json.dumps(data),
        headers={'Content-Type': 'application/json'}
    )

    nades = json.loads(r.text)

    setpos_list = []
    for i in range(0,len(nades)):
        pos_temp = nades[i]
        pos_temp = pos_temp.get('throwAng')
        setpos_list.append(f'setpos {x} {y} {z}; setang {pos_temp[0]} {pos_temp[1]} {pos_temp[2]}')
    list_id[id] = {"setpos":setpos_list,"loaded":False, 'map':map}
    pickle.dump(list_id, open( name, "wb" ) )

#Example of function
pickle_list_id('de_cache', 1318.96875,1055.00122,1704.839965)