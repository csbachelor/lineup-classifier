import json
import requests

def find_pos_id(map, x, y, z):
    ###### load starting positions for a map
    data = {"mapName": map}
    r = requests.post(
        'https://scope.gg/api/grenades/getPredefinedPositions',
        data=json.dumps(data),
        headers={'Content-Type': 'application/json'}
    )

    positions = json.loads(r.text)
    out=None
    out_value=1000000 #Big number
    for i in range(0,len(positions)):
        pos_temp = positions[i]
        if (pos_temp.get('x')-x)**2+(pos_temp.get('y')-y)**2 + (pos_temp.get('z')-z)**2 < out_value:
            out = pos_temp.get('id')
            out_value = (pos_temp.get('x')-x)**2+(pos_temp.get('y')-y)**2 + (pos_temp.get('z')-z)**2

    if out:
        return out
    else:
        return None

#Example of function
xpos = -1559.999755859375
ypos = -728.7709350585938
zpos = 52.242156982421875
used_id = find_pos_id('de_overpass',xpos,ypos,zpos)