import requests
from PIL import Image
from io import BytesIO
from src.picture_crop import *
import pickle
import time
from tqdm import tqdm



def get_data(list_id, out_path, out_dim=[50,50]):
    '''
    list_id: Dict of setpos strings  
    out_path: Relative path to output folder
    '''
    for id in list_id:
        if list_id[id]["loaded"]:
            continue
        setpos = list_id[id]['setpos']
        map = list_id[id]['map']
        with tqdm(total=len(setpos), desc='get_data') as pbar:
            for i, pos in enumerate(setpos):
                pos = pos.split(';')
                ang = pos[1].split(' ')
                pos = pos[0].split(' ')

                link = (
                    f'https://screenshoter.scope.services/'
                    f'{map}?'
                    f'x={pos[1]}&y={pos[2]}&z={pos[3]}'
                    f'&pitch={ang[2]}&yaw={ang[3]}'
                    
                )
                r = requests.get(link)
                img = Image.open(BytesIO(r.content))
                img = picture_crop(img,out_dim[0],out_dim[1])
                img.save(f'{out_path}/{id}_lnop_{i}.png','PNG')
                #time.sleep(.5)
                pbar.update()
        list_id[id]['loaded']=True
        pickle.dump(list_id, open( 'save.p', "wb" ) )

            



list_id = pickle.load(open('save.p','rb'))
get_data(list_id, "data")