import os
import json
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import lines



with open('predictions_dict.json',"r+") as json_file:
    preds = json.load(json_file)

sns.set(color_codes=True)
sns.set(style="darkgrid")



histo = []

for i in preds:
    histo.append(preds[i])

plt.xticks(np.arange(0, 1, step=0.1))
histogram = sns.distplot(histo, bins=10,color="steelblue")
plt.xlabel("Prediction probabilities", fontsize=16)  
plt.ylabel("Density", fontsize=16)
plt.axvline(0.55,color="red")
lgd = []
lgd.append(lines.Line2D([0,0],[0,1], color = 'red', label = 'Threshold t'))
plt.legend(handles = lgd)
plt.show()