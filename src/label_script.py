from PIL import Image, ImageDraw
import os
import json
from picture_crop import picture_crop
dir = "data"
reverse = False
dir_content = os.listdir(dir)
with open('label_dict.json',"r+") as json_file:
    labels = json.load(json_file)
dir_content = [file for file in dir_content if file not in labels.keys()]
if reverse:
    dir_content.reverse()
for image in dir_content:
    if image.endswith(".png"):
        im = Image.open(os.path.join(dir,image))
        im = picture_crop(im,48,48)
        #width, height = im.size
        #draw = ImageDraw.Draw(im)
        #draw.rectangle(((width-20)//2, (height-20)//2, (width+20)//2, (height+20)//2), outline=(0, 0, 0))
        im.show()
        lineup = input(f'{image} lineup? 1 for yes, 0 for no (other to stop):')
        if lineup != "1" and lineup != "0":
            break
        labels[image]=bool(int(lineup))

with open('label_dict.json',"w+") as json_file:
    json.dump(labels,json_file)



'''
for i in range(0,691):
    labels[f'lnop_{i}.png']=bool(0)

'''