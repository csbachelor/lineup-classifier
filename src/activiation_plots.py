import matplotlib.pyplot as plt

x = [-8,0,0,8] 
y = [0, 0,
     0, 1]
plt.grid("Grey")

plt.step(x, y)
plt.show()

import numpy as np 
import math 
plt.grid("Grey")
x = np.linspace(-8, 8, 100) 
z = 1/(1 + np.exp(-x)) 
  
plt.plot(x, z) 
plt.xlabel("x") 
plt.ylabel("Sigmoid(x)") 
  
plt.show() 


def relu(x):
    return np.maximum(0, x)
plt.grid("Grey")

x = np.linspace(-8,8, 100)
y = relu(x)
plt.plot(x, y)
plt.ylim(-1, 8, 2)
plt.show()

def relu(x):
    return np.maximum(x*0.05, x)
plt.grid("Grey")

x = np.linspace(-8,8, 100)
y = relu(x)
plt.plot(x, y)
plt.ylim(-1, 8, 2)
plt.show()