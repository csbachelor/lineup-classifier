import torch
import torch.nn.functional as F
import json
import os
import wandb

from tqdm import tqdm
from rumskib import Net
from skimage import io, util
from picture_crop import picture_crop


hyperparameter_defaults = dict(
dropout_1 = 0.21,
dropout_2 = 0.7093,
dropout_3 = 0.4972,
dropout_4 = 0.722,
dropout_5 = 0.5342,
dropout_d = 0.8966,
channels_1 = 64,
channels_2 = 128,
channels_3 = 256,
channels_4 = 512,
channels_5 = 512,
channels_d = 4096,
batch_size = 32,
learning_rate = 0.06081,
epochs = 128,
)

dir = "data"
dir_content = os.listdir(dir)

wandb.init(config = hyperparameter_defaults,project="bachelor")
config = wandb.config

with open('predictions_dict.json',"r+") as json_file:
    predictions = json.load(json_file)
dir_content = [file for file in dir_content if file not in predictions.keys()]

with open('label_dict.json',"r+") as json_file:
    labels = json.load(json_file)
dir_content = [file for file in dir_content if file not in labels.keys()]

with tqdm(total=len(dir_content), desc='Images') as pbar:
    for image in dir_content:
        if image.endswith(".png"):
            im = os.path.join(dir,image)
            im = io.imread(im)
            H,W,C = im.shape
            im = util.crop(im,(((H-48)/2,(H-48)/2),((W-48)/2,(W-48)/2),(0,0)))
            im = torch.from_numpy(im)
            im = im.unsqueeze(0)

            model = Net(config).to("cpu")
            state = torch.load("src/model.pt", map_location="cpu")
            model.load_state_dict(state)
            model.eval()
            preds = model(im).view(-1).float()
            preds = F.sigmoid(preds)
            print(preds)
            predictions[image] = preds.item()
            with open('predictions_dict.json',"w+") as json_file:
                json.dump(predictions,json_file)
        pbar.update()