import os
import argparse
from tqdm import tqdm
import pprint
import copy

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as D
import torchvision.transforms as T
from torch.utils.data import DataLoader
from dataloader import LineupDataset, DummyDataset
from torchvision.utils import save_image, make_grid
import matplotlib.pyplot as plt
import wandb
import seaborn as sn
import pandas as pd
from sklearn.model_selection import KFold

hyperparameter_defaults = dict(
dropout_1 = 0.21,
dropout_2 = 0.7093,
dropout_3 = 0.4972,
dropout_4 = 0.722,
dropout_5 = 0.5342,
dropout_d = 0.8966,
channels_1 = 64,
channels_2 = 128,
channels_3 = 256,
channels_4 = 512,
channels_5 = 512,
channels_d = 4096,
batch_size = 32,
learning_rate = 0.06081,
epochs = 128,
)


parser =argparse.ArgumentParser()
# actions
parser.add_argument('--train', action='store_true', help='Train a new or restored model.')
parser.add_argument('--evaluate', action='store_true', help='Evaluate a model.')
parser.add_argument('--generate', action='store_true', help='Generate a plot.')
parser.add_argument('--CV', type=int, help='Does k fold cross validation')
parser.add_argument('--cuda', type=int, help='Which cuda device to use')
parser.add_argument('--seed', type=int, default=1, help='Random seed.')

# file paths
parser.add_argument('--restore_file', type=str, help='Path to model to restore.')
parser.add_argument('--data_dir', default='./data/', help='Location of dataset.')
parser.add_argument('--dummy', action='store_true', help='Generate and train using black/white images.')
parser.add_argument('--output_dir', default='./results/{}'.format(os.path.splitext(__file__)[0]))
parser.add_argument('--results_file', default='results.txt', help='Filename where to store settings and test results.')




# --------------------
# Data
# --------------------


def fetch_dataloaders(args):

    transforms = T.Compose([T.ToTensor()])
    kwargs = {'num_workers': 1, 'pin_memory': True} if args.device.type == 'cuda' else {}

    #Fix
    def get_dataset(args):
        if args.dummy:
            return DummyDataset(root_dir=args.data_dir,n=100)
        else:
            return LineupDataset(json_file='label_dict.json',root_dir=args.data_dir)


    def get_dl(dataset, shuffle):
        return DataLoader(dataset, batch_size=args.batch_size, shuffle=shuffle, drop_last=True)#, **kwargs)
    
    full_dataset = get_dataset(args)
    train_size = int(0.8 * len(full_dataset))
    test_size = len(full_dataset) - train_size
    train_dataset, test_dataset = torch.utils.data.random_split(full_dataset, [train_size, test_size])

    return get_dl(train_dataset,True), get_dl(test_dataset,False)


# --------------------
# Model
# --------------------

class Net(nn.Module):
    
    def __init__(self,config):
        super().__init__()

        conv_layers = [3]+[config.channels_1]*2
        conv_kernelsize = [3,3]
        conv_stride = [1,1]
        conv_padding = [1,1]
        conv_activation = [nn.ReLU,nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_1]*2

        
        self.conv1 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout)

        conv_layers = [config.channels_1]+[config.channels_2]*2 
        conv_kernelsize = [3,3]
        conv_stride = [1,1]
        conv_padding = [1,1]
        conv_activation = [nn.ReLU,nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_2]*2
        self.conv2 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout)
        conv_layers = [config.channels_2]+[config.channels_3]*3 
        conv_kernelsize = [3,3,3]
        conv_stride = [1,1,1]
        conv_padding = [1,1,1]
        conv_activation = [nn.ReLU,nn.ReLU, nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_3]*3
        self.conv3 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout)  
        conv_layers = [config.channels_3]+[config.channels_4]*3 
        conv_kernelsize = [3,3,3]
        conv_stride = [1,1,1]
        conv_padding = [1,1,1]
        conv_activation = [nn.ReLU,nn.ReLU, nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_4]*3
        self.conv4 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout) 
        conv_layers = [config.channels_4]+[config.channels_5]*3 
        conv_kernelsize = [3,3,3]
        conv_stride = [1,1,1]
        conv_padding = [1,1,1]
        conv_activation = [nn.ReLU,nn.ReLU, nn.ReLU]
        conv_pool = nn.MaxPool2d
        conv_pool_size = 2
        conv_pool_stride = 2
        conv_pool_padding = 0
        conv_dropout = [config.dropout_5]*3
        self.conv5 = self.create_convolutional_layer(layers=conv_layers, kernel_sizes=conv_kernelsize, strides=conv_stride,
                                                    paddings=conv_padding, activation_functions=conv_activation, pool_kernel_size=conv_pool_size,
                                                    pool_stride=conv_pool_stride, pool_padding=conv_pool_padding,
                                                    dropouts=conv_dropout)                             
        
        self.features_cnn = conv_layers[-1] * 1*1
        
        # fully connected output layers
        
        fc_layers = [self.features_cnn]+[config.channels_d]*2 #[self.features_cat_size, 4096, 4096, 1024, self.num_classes]
        fc_activations = [nn.ReLU, nn.ReLU] #[nn.ReLU, nn.ReLU, nn.ReLU, nn.Identity]
        fc_biases = [True,True] #[True, True, False, False]
        fc_normfuncs = [nn.BatchNorm1d,nn.BatchNorm1d]
        fc_dropouts = [config.dropout_d]*2 #[0.4, 0.3] #[0.5, 0.5, 0.5, 0.5]
        
        self.fc = self.create_fully_connected_layer(layers=fc_layers, activation_functions=fc_activations, 
                                                    bias_bools=fc_biases, normfuncs=fc_normfuncs, dropouts=fc_dropouts)
        self.fc_out = nn.Linear(fc_layers[-1],1)

    @staticmethod
    def create_fully_connected_layer(layers, activation_functions, bias_bools, normfuncs, dropouts):
        fc = nn.Sequential()
        for idx, _ in enumerate(layers):
            if idx == 0:
                continue
            # Extract the in and out channel numbers and layer specifications
            in_channel = layers[idx-1]
            out_channel = layers[idx]
            activation_function = activation_functions[idx-1]
            bias_bool = bias_bools[idx-1]
            normfunc = normfuncs[idx-1]
            dropout = dropouts[idx-1]
            
            fc.add_module('fc{}'.format(idx),
                nn.Sequential(
                    nn.Dropout(dropout), # dropout (before linear function as in Srivastava et al, 2014)
                    nn.Linear(in_channel, out_channel, bias=bias_bool),
                    normfunc(out_channel), # batch normalization before activation function as suggested in Ioffe and Szegedy 2015
                    activation_function(inplace=True)
                )
            )
        
        return fc
    
    
    @staticmethod
    def create_convolutional_layer(layers, kernel_sizes, strides, paddings, activation_functions, pool_kernel_size, pool_stride, pool_padding, dropouts):
        conv = nn.Sequential()
        
        for idx, _ in enumerate(layers):
            if idx == 0:
                continue
            # Extract the in and out channel numbers and layer specifications
            in_channels = layers[idx-1]
            out_channels = layers[idx]
            kernel_size = kernel_sizes[idx-1]
            stride = strides[idx-1]
            padding = paddings[idx-1]
            activation_function = activation_functions[idx-1]
            dropout = dropouts[idx-1]
            
            conv.add_module('conv{}'.format(idx),
                nn.Sequential(
                    nn.Dropout(dropout),
                    nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size, stride=stride, padding=padding),
                    nn.BatchNorm2d(out_channels), # batch normalization before activation function as suggested in Ioffe and Szegedy 2015
                    activation_function(inplace=True)
                )
            )
        
        conv.add_module('maxpool',nn.MaxPool2d(kernel_size=pool_kernel_size, stride=pool_stride, padding=pool_padding))
        return conv
    
    def forward(self, x):
        # Your code here!
        
        ## transform the input
        #x = self.stn(x)
        ## save transformation
        #l_trans1 = Variable(x.data)

        # Perform the usual forward pass
        
        # Convolutional network
        x = x.permute(0,3,1,2)
        x = self.conv1(x.float())
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = x.view(-1,self.features_cnn)
        x = self.fc(x)
        # return output and batch of bilinear interpolated images
        return self.fc_out(x)




# --------------------
# Train and eval
# --------------------

def train_epoch(model, train_dataloader, optimizer,criterion, epoch, args):
    model.train()

    n_batches = len(train_dataloader)
    train_dataloader_batch = iter(train_dataloader)
    accurate_preds = 0
    with tqdm(total=n_batches, desc='epoch {} of {}'.format(epoch+1, args.n_epochs)) as pbar:
        for i in range(n_batches):

            # get batch from respective dataloader
            sample = train_dataloader_batch.next()
            y = sample['label'].float().to(args.device)
            x = sample['image'].to(args.device)

            optimizer.zero_grad()
            output = model(x).view(-1).float()
            loss = criterion(output, y)
            loss.backward()
            optimizer.step()
            preds = torch.sigmoid(output)
            preds = torch.round(preds)
            accurate_preds += (preds == y).sum().item()
            # update trackers
            pbar.set_postfix(loss='{:.3f}'.format(loss.item()))
            pbar.update()
    accuracy = accurate_preds / (args.batch_size * len(train_dataloader))
    return accuracy


@torch.no_grad()
def evaluate(model, dataloader, epoch, args):
    model.eval()
    fp = 0
    fn = 0
    tp = 0
    tn = 0
    loss=0
    with tqdm(total=len(dataloader), desc='eval') as pbar:
        for i, sample in enumerate(dataloader):
            y = sample['label'].float().to(args.device)
            x = sample['image'].to(args.device)
            preds = model(x).view(-1).float()
            preds = torch.sigmoid(preds)
            loss += criterion(preds, y)
            #print([['%.3f' % n for n in preds]])
            preds = torch.round(preds)
            tp += ((preds == y) & (y==1)).sum().item()
            tn += ((preds == y) & (y==0)).sum().item()
            fp += (preds > y).sum().item()
            fn += (preds < y).sum().item()

            pbar.set_postfix(accuracy='{:.4f}'.format((tn+tp) / ((i+1) * args.batch_size)))
            pbar.update()


    accuracy = (tn+tp) / (args.batch_size * len(dataloader))
    loss = loss / len(dataloader)
    if epoch:
        output = (epoch != None)*'Epoch {} -- '.format(epoch+1) + 'Test set accuracy: {:.4f}'.format(accuracy)
    else:
        output = 'Test set accuracy: {:.4f}'.format(accuracy)
    #generate(tn,tp,fn,fp, args)
    print(output)
    print(output, file=open(args.results_file, 'a'))
    return accuracy, loss, tn, tp, fn, fp

def train_and_evaluate(model, train_dataloader, test_dataloader, optimizer,criterion, args):
    out = []
    for epoch in range(args.n_epochs):
        train_accuracy=train_epoch(model, train_dataloader, optimizer,criterion, epoch, args)
        test_accuracy, test_loss, tn, tp, fn, fp=evaluate(model, test_dataloader, epoch, args)
        # save weights
        torch.save(model.state_dict(), os.path.join(args.output_dir, 'LineupClassifier'))
        torch.save(model.state_dict(), os.path.join(wandb.run.dir, 'model.pt'))
        wandb.log({"train_accuracy":train_accuracy,"test_accuracy": test_accuracy, "test_loss": test_loss})

        # show samples -- SSL paper Figure 1-b
        dict1 = {"epoch":epoch,"value":train_accuracy,"measure":"Train accuracy"}
        dict2 = {"epoch":epoch,"value":test_accuracy,"measure":"Testing accuracy"}
        dict3 = {"epoch":epoch,"value":test_loss.data.cpu().numpy(),"measure":"Testing loss"}

        out.append(dict1)
        out.append(dict2)
        out.append(dict3)

        # if epoch % 10 == 0:
    return train_accuracy,test_accuracy,test_loss, out


# --------------------
# Visualize
# --------------------

@torch.no_grad()
def generate(tn,tp,fn,fp, args):
    array = [[tp,fn],[fp,tn]]

    df_cm = pd.DataFrame(array, range(2), range(2))
    # plt.figure(figsize=(10,7))
    sn.set(font_scale=1.4) # for label size
    sn.heatmap(df_cm, annot=True, annot_kws={"size": 16}) # font size
    plt.xlabel('Predicted class')
    plt.ylabel('Actual class')
    plt.tight_layout()
    plt.savefig(os.path.join(args.output_dir, 'conf_matrix.png'))
    
@torch.no_grad()
def roc_plot(model, dataloader, args):
    fp_rate = []
    tp_rate = []
    y = []
    predictions = []
    for i, sample in enumerate(dataloader):
        y+=sample['label'].numpy().tolist()
        x = sample['image'].to(args.device)
        preds = model(x).view(-1).float()
        predictions+=torch.sigmoid(preds).to("cpu").numpy().tolist()
    P = sum(y)
    N = len(y)-P
    for i in range(20):
        threshold = 0.05*i
        FP = 0
        TP = 0
        for i in range(len(y)):
            if (predictions[i]> threshold):
                if y[i] == 1:
                    TP+=1
                if y[i] == 0:
                    FP+=1
        fp_rate.append(FP/float(N))
        tp_rate.append(TP/float(P))
    plt.figure()
    plt.plot(fp_rate,tp_rate)
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.scatter(fp_rate[10],tp_rate[10], color='red')
    plt.tight_layout()
    plt.grid(True)
    plt.savefig(os.path.join(args.output_dir, 'roc.png'))

@torch.no_grad()
def worst_off_plot(model, dataloader, args):
    model.eval()
    fig = plt.figure(figsize=(18,30))
    pic = 0
    for i in range(dataloader.dataset.__len__()):
        sample = dataloader.dataset[i]
        y = sample['label']
        
        x = torch.from_numpy(sample['image']).to(args.device).unsqueeze(0)
        preds = model(x).view(-1).float()
        prediction=torch.sigmoid(preds).to("cpu").numpy().tolist()
        diff = abs(prediction[0] - y)
        if diff > 0.65:
            pic +=1
            plt.subplot(5,4,pic)
            plt.imshow(sample['image'])
            plt.title(f'Label:{y} | Prediction: {prediction[0]:.2f}', fontsize=20)
            plt.axis('off')
    plt.savefig(os.path.join(args.output_dir,'wrong_pred.png'))

# --------------------
# Main
# --------------------

if __name__ == '__main__':
    args, unknown = parser.parse_known_args()
    wandb.init(config = hyperparameter_defaults,project="bachelor")
    config = wandb.config
    args.batch_size = config.batch_size
    args.lr = config.learning_rate
    args.n_epochs = config.epochs
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)

    args.device = torch.device('cuda' if torch.cuda.is_available() and args.cuda != None else 'cpu')
    torch.manual_seed(args.seed)
    if args.device.type == 'cuda': torch.cuda.manual_seed(args.seed)


    
    

    # optimizer
    
    #optimizer = torch.optim.SGD(model.parameters(), lr = args.lr, momentum= 0.9)
    
    
    # dataloaders
    train_dataloader, test_dataloader = fetch_dataloaders(args)

    # model
    model = Net(config).to(args.device)
    if args.restore_file:
        # load model and optimizer states
        state = torch.load(args.restore_file, map_location=args.device)
        model.load_state_dict(state)
        # set up paths
        args.output_dir = os.path.dirname(args.restore_file)
    args.results_file = os.path.join(args.output_dir, args.results_file)

    print('Loaded settings and model:')
    print(pprint.pformat(args.__dict__))
    print(model)
    print(pprint.pformat(args.__dict__), file=open(args.results_file, 'a'))
    print(model, file=open(args.results_file, 'a'))

    if args.train:
        wandb.watch(model)
        _ = train_and_evaluate(model, train_dataloader, test_dataloader, optimizer, criterion, args)

    if args.evaluate:
        evaluate(model, test_dataloader, None, args)

    if args.generate:
        roc_plot(model, test_dataloader, args)
        worst_off_plot(model,test_dataloader,args)
    if args.CV:
        dataset = LineupDataset(json_file='label_dict.json',root_dir=args.data_dir)
        cv = KFold(n_splits=args.CV,shuffle=True)
        df = pd.DataFrame(columns=("epoch","value","measure"))
        for train,test in cv.split(dataset):
            model = Net(config).to(args.device)
            criterion = nn.BCEWithLogitsLoss()
            optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
            train_set = torch.utils.data.Subset(dataset,train)
            test_set = torch.utils.data.Subset(dataset,test)
            train_dataloader = DataLoader(train_set, batch_size=args.batch_size, shuffle=True, drop_last=True)
            test_dataloader = DataLoader(test_set, batch_size=args.batch_size, shuffle=False, drop_last=True)
            train_accuracy,test_accuracy,test_loss, out = train_and_evaluate(model, train_dataloader, test_dataloader, optimizer, criterion, args)
            df = df.append(pd.DataFrame(out))
        df['epoch'] = df['epoch'].astype('int')
        df['value'] = df['value'].astype('float')
        sn.lineplot('epoch','value', hue = 'measure',data=df)
        plt.savefig(os.path.join(args.output_dir,'accuracy.png'))
