from torch.utils.data import Dataset
import json
import os
from skimage import io, util
import torch
import random

class DummyDataset(Dataset):
    def __init__(self,root_dir, n):
        self.root_dir = root_dir
        self.images = ['black']*n+['white']*n

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        if self.images[idx]=='black':
            img_name = os.path.join(self.root_dir,'black.jpg')
            label = 1
        else:
            img_name = os.path.join(self.root_dir,'white.jpg')
            label = 0
        image = io.imread(img_name)
        H,W,C = image.shape
        image = util.crop(image,(((H-50)/2,(H-50)/2),((W-50)/2,(W-50)/2),(0,0)))
        sample = {'image':image, "label":label}
        return sample


class LineupDataset(Dataset):

    def __init__(self,json_file, root_dir):
        with open(json_file,'r+') as json_file2:
            self.labels = json.load(json_file2)
        self.root_dir = root_dir
        positive = {image:label for image,label in self.labels.items() if label == 1}
        negative = {image:label for image,label in self.labels.items() if label == 0}
        #Remove alot of negative images.
        random.seed(30)
        for image in random.sample(negative.keys(),len(negative)-len(positive)):
            del negative[image]
        positive.update(negative)
        self.images = list(positive.keys())
    
    def __len__(self):
        return len(self.images)

    def __getitem__(self,idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
    
        img_name = os.path.join(self.root_dir,self.images[idx])
        image = io.imread(img_name)
        H,W,C = image.shape
        image = util.crop(image,(((H-48)/2,(H-48)/2),((W-48)/2,(W-48)/2),(0,0)))
        sample = {'image':image, "label":int(self.labels[self.images[idx]])}
        return sample
