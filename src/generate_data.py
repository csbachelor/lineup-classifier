from PIL import ImageGrab, Image
import os
from src.picture_crop import *
import pyautogui
import pickle
import time

def generate_data(setpos,cs_path, out_path, out_dim=[50,50]):
    '''
    setpos: List of setpos strings  
    cs_path: Absolute path to cs folder
    out_path: Relative path to output folder
    '''
    time.sleep(3)

    for i, pos in enumerate(setpos):
        file = open(f'{cs_path}/csgo/cfg/setpos_exec.cfg','w+')
        file.truncate() #Empty file
        file.write(pos) #Write new position
        file.close()
        time.sleep(0.1)

        pyautogui.press('u') #Bound in csgo to 'exec setpos_exec'

        pyautogui.press('prtscr') #Bound in csgo to 'jpeg screenshot'
        
        img = ImageGrab.grabclipboard() #Pulls image from clipboard
        ###Cut the image down before saving to file to minimize storage needed.
        img = picture_crop(img,out_dim[0],out_dim[1])
        img.save(f'{out_path}/lnop_{i}.png','PNG')
        #os.remove(f'{cs_path}/csgo/screenshots/screenshot.jpg')

cs_path='C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Global Offensive'
setpos = pickle.load(open('save.p','rb'))

generate_data(setpos[0:9],cs_path,out_path="data")
