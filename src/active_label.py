from PIL import Image, ImageDraw
import os
import json
from picture_crop import picture_crop
dir = "data"


with open('predictions_dict.json',"r+") as json_file:
    preds = json.load(json_file)

with open('label_dict.json',"r+") as json_file:
    labels = json.load(json_file)

preds_2 = {k:(abs(v-0.5)) for k, v in preds.items()}
sorted_preds = {k: v for k, v in sorted(preds_2.items(), key=lambda item: item[1])}


sorted_preds = [file for file in sorted_preds.keys() if file not in labels.keys()]
for image in sorted_preds:
    if image.endswith(".png"):
        im = Image.open(os.path.join(dir,image))
        im = picture_crop(im,48,48)
        im.show()
        lineup = input(f'{image} lineup? 1 for yes, 0 for no (other to stop):')
        if lineup != "1" and lineup != "0":
            break
        labels[image]=bool(int(lineup))
        with open('label_dict.json',"w+") as json_file:
            json.dump(labels,json_file) 
