# json package
library("rjson")

person1 <- fromJSON(file = "C:/Users/Rnd/Documents/BachelorCode/lineup-classifier/F1score/person1.json")
person2 <- fromJSON(file = "C:/Users/Rnd/Documents/BachelorCode/lineup-classifier/F1score/person2.json")

person1 = person1[order(names(person1))]
person2 = person2[order(names(person2))]


df1 <- cbind(as.integer(t(as.data.frame(person1))[,1]))
df2 <- cbind(as.integer(t(as.data.frame(person2))[,1]))

TP = 0
FN = 0
FP = 0

for (i in 1:100){
   if (df1[i]==1 & df2[i]==1){
       TP = TP+1
   }
   else if (df1[i]==1 & df2[i]==0){
       FN = FN+1
   }
   else if (df1[i]==0 & df2[i]==1){
       FP = FP+1
   }

}

precision = TP / (TP + FP)
recall = TP / (TP + FN)
score = 2 * ((precision*recall)/(precision+recall))

print(score)